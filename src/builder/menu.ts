/** Dependencies */
import {
    Collection,
    Components,
    Section,
    each,
    findFirst,
    populateSlots,
} from "@tripetto/builder";
import { Setter } from "./";
import { Value } from "./value";
import { getIcon } from "./icon";

export const variablesMenu = (
    setter: Setter,
    values: Collection.Provider<Value, Setter>,
    isAdvanced: boolean
) => {
    const menu: Components.MenuOption[] = [];
    let section: string | undefined;

    const addSection = (src: Section | undefined) => {
        if (src?.id !== section) {
            const name = src?.name;

            if (menu.length > 0 || name) {
                menu.push(new Components.MenuSeparator());
            }

            if (name) {
                menu.push(new Components.MenuLabel(name));
            }

            section = src?.id;
        }
    };

    const isUsed = (id: string) =>
        !isAdvanced && findFirst(values.all, (value) => value.variable === id)
            ? true
            : false;

    each(
        populateSlots(setter, {
            pipes: "exclude",
        }),
        (identifier) => {
            if (identifier.slots) {
                const children: Components.MenuOption[] = [];

                each(identifier.slots, (slot) => {
                    if (
                        slot.type === "slot" &&
                        slot.id &&
                        slot.slot &&
                        !slot.slot.protected
                    ) {
                        children.push(
                            new Components.MenuItemWithImage(
                                getIcon(slot.slot),
                                slot.label,
                                () => values.append().attachVariable(slot.id!),
                                isUsed(slot.id!)
                            )
                        );
                    }
                });

                if (children.length > 0) {
                    addSection(identifier.block.node.section);

                    menu.push(
                        new Components.MenuSubmenuWithImage(
                            identifier.icon || identifier.block.type.icon,
                            identifier.label,
                            children
                        )
                    );
                }
            } else if (
                identifier.id &&
                identifier.slot &&
                !identifier.slot.protected
            ) {
                addSection(identifier.block.node.section);

                menu.push(
                    new Components.MenuItemWithImage(
                        identifier.icon || identifier.block.type.icon,
                        identifier.label,
                        () => values.append().attachVariable(identifier.id!),
                        isUsed(identifier.id!)
                    )
                );
            }
        }
    );

    return menu;
};
